# Protocols

Protocols is a JSON based library to provide type based serialization for network communications.

## Usage Go

Send structure.

```go
import "github.com/axet/protocols/go"

type EMailLogin struct {
  EMail string
  Password string
}

func main() {
  protocol.Encode(w, EMailLogin{EMail: "example@example.com", Password: "password"})
}
```

Output:

```json
{ "EMailLogin": { "EMail" : "example@example.com", "Password" : "password"}
```

Receive stucture dynamic typing

```go
func login(w http.ResponseWriter, r *http.Request) {
    packet, err := protocol.Decode(r.Body, []interface{}{FacebookLogin{},
      EMailLogin{}})
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }
    
    switch packet.(type) {
    case *FacebookLogin:
      fb := packet.(*FacebookLogin)
    case *EMailLogin:
      email := packet.(*EMailLogin)
    default:
      http.Error(w, "Bad Login", http.StatusInternalServerError)
      return
    }
}
```

## Usage Java

```java
package com.github.axet.protocols;

import java.io.Reader;
import java.util.HashMap;

public class Test {

    public interface Action {
        public void run(Object o);
    }

    public static class Login {
        public String login;
        public String password;
    }

    public static class Share {
        public String text;
        public String description;
    }

    HashMap<Object, Action> commands = new HashMap<Object, Action>() {
        private static final long serialVersionUID = 2346281120437817249L;

        {
            put(Login.class, new Action() {
                @Override
                public void run(Object o) {
                    login((Login) o);
                }
            });
            put(Share.class, new Action() {
                @Override
                public void run(Object o) {
                    share((Share) o);
                }
            });
        }
    };

    public void login(Login o) {
    }

    public void share(Share s) {
    }

    Decoder decoder = new Decoder(new Class<?>[] { Login.class, Share.class });

    public void run() {
        while (true) {
            Reader httpbody = null;

            decoder.setDecoder(httpbody);
            Object o = decoder.decode();
            Action c = commands.get(o);
            c.run(o);
        }
    }

    public static void main(String[] args) {
    }

}
```
