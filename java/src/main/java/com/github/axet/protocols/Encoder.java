package com.github.axet.protocols;

import java.io.IOException;
import java.io.Writer;
import java.util.TreeMap;

import com.google.gson.Gson;

public class Encoder {

    Writer encoder;

    public void setEncoder(Writer r) {
        encoder = r;
    }

    public void encode(Object o) {
        Gson gson = Protocol.gson();

        TreeMap<String, Object> map = new TreeMap<String, Object>();
        map.put(o.getClass().getSimpleName(), o);

        String s = gson.toJson(map);

        try {
            encoder.write(s);
            encoder.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void encode(Object o, Writer r) {
        Encoder e = new Encoder();
        e.setEncoder(r);
        e.encode(o);
    }
   
}
