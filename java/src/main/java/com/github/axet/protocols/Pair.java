package com.github.axet.protocols;

public class Pair {
    public String name;
    public Class<?> klass;

    public Pair() {
    }

    public Pair(String name, Class<?> klass) {
        this.name = name;
        this.klass = klass;
    }
}