package com.github.axet.protocols;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class Protocol {

    static final String RFC3339 = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSSZZ";
    static final String UTF8 = "UTF8";

    public static class DateSerializer implements JsonSerializer<Date>, JsonDeserializer<Date> {
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {

            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern(RFC3339);
            String d = dateFormatter.print(new DateTime(src));

            return new JsonPrimitive(d);
        }

        @Override
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {

            DateTimeFormatter dateFormatter = DateTimeFormat.forPattern(RFC3339);
            Date d = dateFormatter.parseDateTime(json.getAsString()).toDate();

            return d;
        }
    }

    public static Gson gson() {
        // set RFC3339 date format

        // Java 7+ only
        // http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
        // String java7 = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSSXXX";

        // Android only
        // http://developer.android.com/reference/java/text/SimpleDateFormat.html
        // String android = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateSerializer()).create();
        return gson;
    }

    public static Object decode(Class<?>[] types, Reader r) {
        Decoder p = new Decoder(types);
        p.setDecoder(r);
        return p.decode();
    }

    public static Object decode(Pair[] types, Reader r) {
        Decoder p = new Decoder(types);
        p.setDecoder(r);
        return p.decode();
    }

    public static Object decode(Class<?>[] types, String r) {
        try {
            Decoder p = new Decoder(types);
            InputStream is = new ByteArrayInputStream(r.getBytes(UTF8));
            p.setDecoder(new InputStreamReader(is));
            return p.decode();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Object decode(Pair[] types, String r) {
        try {
            Decoder p = new Decoder(types);
            InputStream is = new ByteArrayInputStream(r.getBytes(UTF8));
            p.setDecoder(new InputStreamReader(is));
            return p.decode();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void encode(Object o, Writer r) {
        Encoder e = new Encoder();
        e.setEncoder(r);
        e.encode(o);
    }

    public static String encode(Object o) {
        try {
            ByteArrayOutputStream bb = new ByteArrayOutputStream();
            Encoder e = new Encoder();
            e.setEncoder(new OutputStreamWriter(bb));
            e.encode(o);
            bb.flush();
            return bb.toString(UTF8);
        } catch (IOException e1) {
            throw new RuntimeException(e1);
        }
    }

}
