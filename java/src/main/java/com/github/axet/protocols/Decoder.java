package com.github.axet.protocols;

import java.io.Reader;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

public class Decoder {

    public static class DecoderException extends RuntimeException {
        public DecoderException(String s) {
            super(s);
        }

        public DecoderException(Throwable e) {
            super(e);
        }
    }

    Map<String, Class<?>> types = new TreeMap<String, Class<?>>();
    JsonReader decoder;

    public Decoder(Class<?>[] types) {
        setTypes(types);
    }

    public Decoder(Pair[] types) {
        setTypes(types);
    }

    public void setTypes(Class<?>[] types) {
        for (Class<?> c : types) {
            this.types.put(c.getSimpleName(), c);
        }
    }

    public void setTypes(Pair[] types) {
        for (Pair c : types) {
            this.types.put(c.name, c.klass);
        }
    }

    public void setDecoder(Reader r) {
        decoder = new JsonReader(r);
    }

    public Object decode() throws DecoderException {
        try {
            Gson gson = Protocol.gson();
            JsonParser parser = new JsonParser();
            JsonObject object1 = parser.parse(decoder).getAsJsonObject();
            for (Map.Entry<String, JsonElement> oo : object1.entrySet()) {
                String klass = oo.getKey();
                Class<?> kls = types.get(klass);
                if (kls == null) {
                    throw new DecoderException("no such input class");
                }
                return gson.fromJson(object1.get(klass), kls);
            }

            throw new DecoderException("bad stream");
        } catch (DecoderException e) {
            throw e;
        } catch (JsonSyntaxException e) {
            throw new DecoderException(e);
        }
    }
}
