package com.github.axet.protocols;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;

public class Encode {

    public static void main(String[] args) {
        try {

            Share s = new Share();
            s.Url = "123";
            s.Date = new Date();

            Encoder p = new Encoder();

            ByteArrayOutputStream bb = new ByteArrayOutputStream();

            p.setEncoder(new OutputStreamWriter(bb));
            p.encode(s);

            bb.flush();

            System.out.println(bb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
