package com.github.axet.protocols;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Decode {

    public static void main(String[] args) {
        try {
            String json = "{\"Share\" : {\"Url\" : \"test\", \"Date\" : \"2014-11-04T21:22:21.691365661-05:00\"}}";

            Decoder p = new Decoder(new Class<?>[] { Share.class });

            InputStream is = new ByteArrayInputStream(json.getBytes("UTF-8"));

            p.setDecoder(new InputStreamReader(is));
            Share o = (Share) p.decode();
            System.out.println(o.Url);
            System.out.println(o.Date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
