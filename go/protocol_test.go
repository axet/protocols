package protocol

import (
  "testing"
  "time"
  "os"
  "strings"
  "fmt"
)

type Share struct {
  Url string
  Date time.Time
}

func TestEncode(test *testing.T) {
  s := Share{Url: "example.com", Date: time.Now()}
  Encode(os.Stdout, s)
}  
  
func TestDecode(test *testing.T) {
  a := `{"Share":{"Url":"example.com","Date":"2014-11-04T21:22:21.691365661-05:00"}}`
  m, err := Decode(strings.NewReader(a), []interface{}{Share{}})
  if err != nil {
    fmt.Println(err)
    return
  }
  fmt.Printf("%+v\n", m)
}

