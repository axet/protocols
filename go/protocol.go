package protocol

import (
  "io"
  "reflect"
  "encoding/json"
  "errors"
)

type Protocol struct {
  types map[string]reflect.Type
  decoder *json.Decoder
  encoder *json.Encoder
}

// NewTypesMap([]interface{}{FacebookPictureRespond{}})
//
func (p *Protocol) NewTypes(types []interface{}) {
  p.types = map[string]reflect.Type{}
  for _, e := range types {
    t := reflect.TypeOf(e)
    p.types[t.Name()] = t
  }
}

// NewTypesMap(map[string]interface{}{"data" : FacebookPictureRespond{}})
//
func (p *Protocol) NewTypesMap(types map[string]interface{}) {
  p.types = map[string]reflect.Type{}
  for k, e := range types {
    t := reflect.TypeOf(e)
    p.types[k] = t
  }
}

//
// Decode
//

func (p *Protocol) NewDecoder(s io.Reader) {
  p.decoder = json.NewDecoder(s)
}

func (p *Protocol) Decode() (interface{}, error) {
  var packet map[string]*json.RawMessage
  if err := p.decoder.Decode(&packet); err != nil {
    return nil, err
  }

  // only one key value pair possible here, take first and exit
  for k := range(packet) {
    t := p.types[k]

    // if packet contain unknown struct name, throw error
    if t == nil {
      continue
    }

    command := reflect.New(t).Interface()
    if err := json.Unmarshal(*packet[k], &command); err != nil {
      return nil, err
    }

    return command, nil
  }
  
  return nil, errors.New("Unable to read packet")
}

// fpr, err := protocol.Decode(r, []interface{}{FacebookPictureRespond{}})
func Decode(s io.Reader, types []interface{}) (interface{}, error) {
  var p Protocol
  p.NewTypes(types)
  p.NewDecoder(s)
  return p.Decode()
}

// fpr, err := protocol.DecodeMap(r, map[string]interface{}{"data" : FacebookPictureRespond{}})
func DecodeMap(s io.Reader, types map[string]interface{}) (interface{}, error) {
  var p Protocol
  p.NewTypesMap(types)
  p.NewDecoder(s)
  return p.Decode()
}

//
// Encode
//

func (p *Protocol) NewEncoder(s io.Writer) {
  p.encoder = json.NewEncoder(s)
}

func (p *Protocol) Encode(i interface{}) (error) {
  t := reflect.TypeOf(i)

  packet := make(map[string]interface{})
  packet[t.Name()] = i

  if err := p.encoder.Encode(packet); err != nil {
    return err
  }
  
  return nil
}

func Encode(s io.Writer, i interface{}) (error) {
  var p Protocol
  p.NewEncoder(s)
  return p.Encode(i)
}
